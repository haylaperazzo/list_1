#include <stdio.h>

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

int main(int argc, char** argv) {
  // Verifica se o ano � bissexto(ano/4 com resto 0).
  int ano;
  printf("Informe o ano que deseja analisar:\n");
  scanf("%d", &ano);
  if(ano > 0) {
    if((ano%4) == 0) {
      printf("Ano bissexto!");
    } else {
      printf("Este ano nao e bisssexto!");
    }
  } else {
    printf("Ano invalido!");
  }
  return 0;
}


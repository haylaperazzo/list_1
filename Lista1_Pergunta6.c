#include <stdio.h>

int main() {
  int r, n, pg, i; 
  printf ("Progressao Geometrica\n\n"); // Uma PG tem como primeiro elemento o num 1, segundo elemento � o elemento anterior (1) * r; 1,2,4,8,16... 
  printf ("Digite a quantidade de elementos(n) da pg: "); 
  scanf ("%d", &n);
  printf("Digite a razao(r) da pg: ");
  scanf("%d", &r);

  pg = 1;
  printf("%d ", pg);
  for( i = 1; i < n; i++ ) {
   pg = pg * r;
   printf("%d ", pg); 
  }
  return 0;
}


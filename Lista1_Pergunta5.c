#include <stdio.h>

int main(int argc, char** argv) {
  int r, n, pa, i; 
  printf ("Progressao Aritmetica\n\n"); // Uma PA tem como primeiro elemento o num 0 + r = segundo elemento; 0,3,6,9,12...
  printf ("Digite a quantidade de elementos(n) da pa: "); 
  scanf ("%d", &n);
  printf("Digite a razao(r) da pa: ");
  scanf("%d", &r);

  for( i = 0; i < n; i++ ) {
   pa =  i * r;
   printf("%d ", pa); 
  }
  return 0;
}

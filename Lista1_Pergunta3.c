#include <stdio.h>

int main(int argc, char** argv) {
  //Fatorial de um n�mero.
  // 5!= 5.4.3.2.1=120
  int num;
  printf("Digite um numero inteiro e positivo:\n");
  scanf("%d", &num);
  if(num >= 0) {
    int fatorial = 1;
    int i;
    for(i = 2; i <= num; i++) {	
      fatorial *= i;    
    }
    printf("O fatorial de %d e %d",num, fatorial);
  } else {
    printf("Opcao invalida!");
  }
  return 0;
}

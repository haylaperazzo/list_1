#include <stdio.h>

int main(int argc, char** argv) { 
	int escolha;
    do {
    	printf("Digite de 1 a 9 para exibir seu menu ou 0 para finalizar seu menu:\n");
    	scanf("%d", &escolha);
    	switch ( escolha ) {	
			  case 0 : 
				  printf("Finalizando menu\n");
          break;
      		case 1 :
    		case 2 :
    		case 3 :
    		case 4 :
    		case 5 :
    		case 6 :    		
    		case 7 :
    		case 8 :
    		case 9 :
        	printf ("Voce escolheu o numero %d\n",escolha);
    			break;
			  default:
    			printf ("Escolha invalida!\n");
    			break;
  		}	  		
	} while (escolha != 0);
	return 0; 
}
